package mocks

import (
	"context"
	"reflect"

	"gitlab.com/unboundsoftware/eventsourced/eventsourced"
)

// Store is a mock implementation of eventsourced.EventStore
type Store struct {
	RestoreSnapshotFunc func(aggregate eventsourced.Aggregate) (eventsourced.Snapshot, error)
	StoreSnapshotFunc   func(aggregate eventsourced.Aggregate, sequenceNo int) error
	GetEventsFunc       func(aggregate eventsourced.Aggregate, sinceSnapshot eventsourced.Snapshot) ([]eventsourced.EventWrapper, error)
	StoreEventFunc      func(
		aggregate eventsourced.Aggregate,
		event eventsourced.Event,
		sequenceNo int,
		newAggregate bool,
	) error
	GetAggregateRootsFunc func(aggregateType reflect.Type) ([]eventsourced.ID, error)
	EventsFunc            func(fromID int, c chan eventsourced.Event, aggregateTypes ...eventsourced.Aggregate) error
	MaxGlobalSeqNoFunc    func(ctx context.Context) (int, error)
}

// RestoreSnapshot mocks eventsourced.EventStore.RestoreSnapshot by calling RestoreSnapshotFunc
// or by returning nil, nil if RestoreSnapshotFunc is nil
func (m *Store) RestoreSnapshot(_ context.Context, aggregate eventsourced.Aggregate) (eventsourced.Snapshot, error) {
	if m.RestoreSnapshotFunc == nil {
		return nil, nil
	}
	return m.RestoreSnapshotFunc(aggregate)
}

// StoreSnapshot mocks eventsourced.EventStore.StoreSnapshot by calling StoreSnapshotFunc
// or by returning nil if StoreSnapshotFunc is nil
func (m *Store) StoreSnapshot(_ context.Context, aggregate eventsourced.Aggregate, sequenceNo int) error {
	if m.StoreSnapshotFunc == nil {
		return nil
	}
	return m.StoreSnapshotFunc(aggregate, sequenceNo)
}

// GetEvents mocks eventsourced.EventStore.GetEvents by calling GetEventsFunc
// or by returning nil, nil if GetEventsFunc is nil
func (m *Store) GetEvents(_ context.Context, aggregate eventsourced.Aggregate, sinceSnapshot eventsourced.Snapshot) ([]eventsourced.EventWrapper, error) {
	if m.GetEventsFunc == nil {
		return nil, nil
	}
	return m.GetEventsFunc(aggregate, sinceSnapshot)
}

// StoreEvent mocks eventsourced.EventStore.StoreEvent by calling StoreEventFunc
// or by returning 0, nil if StoreEventFunc is nil
func (m *Store) StoreEvent(
	_ context.Context,
	aggregate eventsourced.Aggregate,
	event eventsourced.Event,
	sequenceNo int,
	newAggregate bool,
) error {
	if m.StoreEventFunc == nil {
		return nil
	}
	return m.StoreEventFunc(aggregate, event, sequenceNo, newAggregate)
}

// GetAggregateRoots mocks eventsourced.EventStore.GetAggregateRoots by calling GetAggregateRootsFunc
// or by returning nil, nil if GetAggregateRootsFunc is nil
func (m *Store) GetAggregateRoots(_ context.Context, aggregateType reflect.Type) ([]eventsourced.ID, error) {
	if m.GetAggregateRootsFunc == nil {
		return nil, nil
	}
	return m.GetAggregateRootsFunc(aggregateType)
}

// Events mocks eventsourced.EventStore.Events by calling EventsFunc
// or by returning nil if EventsFunc is nil
func (m *Store) Events(_ context.Context, fromID int, c chan eventsourced.Event, aggregateTypes ...eventsourced.Aggregate) error {
	if m.EventsFunc == nil {
		return nil
	}
	return m.EventsFunc(fromID, c, aggregateTypes...)
}

func (m *Store) MaxGlobalSequenceNo(ctx context.Context) (int, error) {
	if m.MaxGlobalSeqNoFunc == nil {
		return 0, nil
	}
	return m.MaxGlobalSeqNoFunc(ctx)
}

var _ eventsourced.EventStore = &Store{}
