package mocks

import (
	"context"
	"reflect"
	"testing"
	"time"

	"github.com/sanity-io/litter"

	"gitlab.com/unboundsoftware/eventsourced/eventsourced"
)

// Publisher is a mock implementation of eventsourced.EventPublisher
type Publisher struct {
	events      []eventsourced.Event
	PublishFunc func(event eventsourced.Event) error
}

// Publish mocks eventsourced.EventPublisher.Publish by calling PublishFunc
// or by adding the published event to the events-slice if PublishFunc is nil
func (p *Publisher) Publish(_ context.Context, event eventsourced.Event) error {
	if p.PublishFunc != nil {
		return p.PublishFunc(event)
	}
	p.events = append(p.events, event)
	return nil
}

type EventModifier func(event eventsourced.Event)

// AssertPublished checks that the expected events were published
func (p *Publisher) AssertPublished(t testing.TB, want []eventsourced.Event, modifiers ...EventModifier) {
	t.Helper()
	if len(want) != 0 || len(p.events) != 0 {
		for _, p := range p.events {
			p.SetWhen(time.Time{})
			for _, f := range modifiers {
				f(p)
			}
		}
		if !reflect.DeepEqual(want, p.events) {
			t.Errorf("Publisher() got = %s, want %s", litter.Sdump(p.events), litter.Sdump(want))
		}
	}
}

var _ eventsourced.EventPublisher = &Publisher{}
