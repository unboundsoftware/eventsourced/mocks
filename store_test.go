package mocks

import (
	"context"
	"errors"
	"fmt"
	"reflect"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"github.com/sanity-io/litter"

	"gitlab.com/unboundsoftware/eventsourced/eventsourced"
)

var tm = time.Date(2020, 11, 23, 10, 32, 12, 0, time.Local)

func TestStore_GetAggregateRoots(t *testing.T) {
	type fields struct {
		GetAggregateRootsFunc func(aggregateType reflect.Type) ([]eventsourced.ID, error)
	}
	type args struct {
		aggregateType reflect.Type
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []eventsourced.ID
		wantErr bool
	}{

		{
			name: "calls func if defined",
			fields: fields{GetAggregateRootsFunc: func(aggregateType reflect.Type) ([]eventsourced.ID, error) {
				wantAggregateType := reflect.TypeOf(&Dummy{})
				if !reflect.DeepEqual(aggregateType, wantAggregateType) {
					t.Errorf("GetAggregateRoots() got = %v, want %v", aggregateType, wantAggregateType)
				}
				return []eventsourced.ID{"abc123"}, nil
			}},
			args: args{
				aggregateType: reflect.TypeOf(&Dummy{}),
			},
			want:    []eventsourced.ID{"abc123"},
			wantErr: false,
		},
		{
			name: "return error from func",
			fields: fields{GetAggregateRootsFunc: func(aggregateType reflect.Type) ([]eventsourced.ID, error) {
				return nil, errors.New("error")
			}},
			args: args{
				aggregateType: reflect.TypeOf(&Dummy{}),
			},
			wantErr: true,
		},
		{
			name:   "default return if func is nil",
			fields: fields{},
			args: args{
				aggregateType: reflect.TypeOf(&Dummy{}),
			},
			want:    nil,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &Store{
				GetAggregateRootsFunc: tt.fields.GetAggregateRootsFunc,
			}
			got, err := m.GetAggregateRoots(context.Background(), tt.args.aggregateType)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetAggregateRoots() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetAggregateRoots() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestStore_GetEvents(t *testing.T) {
	type fields struct {
		GetEventsFunc func(aggregate eventsourced.Aggregate, sinceSnapshot eventsourced.Snapshot) ([]eventsourced.EventWrapper, error)
	}
	type args struct {
		aggregate     eventsourced.Aggregate
		sinceSnapshot eventsourced.Snapshot
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []eventsourced.EventWrapper
		wantErr bool
	}{
		{
			name: "calls func if defined",
			fields: fields{GetEventsFunc: func(aggregate eventsourced.Aggregate, sinceSnapshot eventsourced.Snapshot) ([]eventsourced.EventWrapper, error) {
				wantAggregate := &Dummy{}
				if !reflect.DeepEqual(aggregate, wantAggregate) {
					t.Errorf("GetEvents() got %+v, want %+v", aggregate, wantAggregate)
				}
				wantSnapshot := &snapshot{
					sequenceNo: 10,
					when:       tm,
				}
				if !reflect.DeepEqual(sinceSnapshot, wantSnapshot) {
					t.Errorf("GetEvents() got %+v, want %+v", sinceSnapshot, wantSnapshot)
				}
				return []eventsourced.EventWrapper{
					{
						Event:               &Added{},
						AggregateSequenceNo: 11,
					},
				}, nil
			}},
			args: args{
				aggregate: &Dummy{},
				sinceSnapshot: &snapshot{
					sequenceNo: 10,
					when:       tm,
				},
			},
			want: []eventsourced.EventWrapper{
				{
					Event:               &Added{},
					AggregateSequenceNo: 11,
				},
			},
			wantErr: false,
		},
		{
			name: "return error from func",
			fields: fields{GetEventsFunc: func(aggregate eventsourced.Aggregate, sinceSnapshot eventsourced.Snapshot) ([]eventsourced.EventWrapper, error) {
				return nil, errors.New("error")
			}},
			args: args{
				aggregate: &Dummy{},
				sinceSnapshot: &snapshot{
					sequenceNo: 10,
					when:       tm,
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name:   "default return if func is nil",
			fields: fields{},
			args: args{
				aggregate: &Dummy{},
				sinceSnapshot: &snapshot{
					sequenceNo: 10,
					when:       tm,
				},
			},
			want:    nil,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &Store{
				GetEventsFunc: tt.fields.GetEventsFunc,
			}
			got, err := m.GetEvents(context.Background(), tt.args.aggregate, tt.args.sinceSnapshot)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetEvents() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetEvents() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestStore_RestoreSnapshot(t *testing.T) {
	type fields struct {
		RestoreSnapshotFunc func(aggregate eventsourced.Aggregate) (eventsourced.Snapshot, error)
	}
	type args struct {
		aggregate eventsourced.Aggregate
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    eventsourced.Snapshot
		wantErr bool
	}{
		{
			name: "calls func if defined",
			fields: fields{RestoreSnapshotFunc: func(aggregate eventsourced.Aggregate) (eventsourced.Snapshot, error) {
				wantAggregate := &Dummy{}
				if !reflect.DeepEqual(aggregate, wantAggregate) {
					t.Errorf("RestoreSnapshot() got %+v, want %+v", aggregate, wantAggregate)
				}
				return &snapshot{
					sequenceNo: 10,
					when:       tm,
				}, nil
			}},
			args: args{aggregate: &Dummy{}},
			want: &snapshot{
				sequenceNo: 10,
				when:       tm,
			},
			wantErr: false,
		},
		{
			name: "return error from func",
			fields: fields{RestoreSnapshotFunc: func(aggregate eventsourced.Aggregate) (eventsourced.Snapshot, error) {
				return nil, errors.New("error")
			}},
			args:    args{aggregate: &Dummy{}},
			want:    nil,
			wantErr: true,
		},
		{
			name:    "default return if func is nil",
			fields:  fields{},
			args:    args{aggregate: &Dummy{}},
			want:    nil,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &Store{
				RestoreSnapshotFunc: tt.fields.RestoreSnapshotFunc,
			}
			got, err := m.RestoreSnapshot(context.Background(), tt.args.aggregate)
			if (err != nil) != tt.wantErr {
				t.Errorf("RestoreSnapshot() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("RestoreSnapshot() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestStore_StoreEvent(t *testing.T) {
	type fields struct {
		StoreEventFunc func(aggregate eventsourced.Aggregate, event eventsourced.Event, sequenceNo int,
			newAggregate bool) error
	}
	type args struct {
		aggregate    eventsourced.Aggregate
		event        eventsourced.Event
		sequenceNo   int
		newAggregate bool
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "calls func if defined",
			fields: fields{StoreEventFunc: func(
				aggregate eventsourced.Aggregate,
				event eventsourced.Event,
				sequenceNo int,
				newAggregate bool,
			) error {
				wantAggregate := &Dummy{}
				if !reflect.DeepEqual(aggregate, wantAggregate) {
					t.Errorf("StoreEvent() got %+v, want %+v", aggregate, wantAggregate)
				}
				wantEvent := &Added{}
				if !reflect.DeepEqual(event, wantEvent) {
					t.Errorf("StoreEvent() got %+v, want %+v", event, wantEvent)
				}
				assert.Equal(t, 1, sequenceNo)
				if !newAggregate {
					t.Errorf("StoreEvent() got %+v, want %+v", newAggregate, true)
				}
				return nil
			}},
			args: args{
				aggregate:    &Dummy{},
				event:        &Added{},
				sequenceNo:   1,
				newAggregate: true,
			},
			wantErr: false,
		},
		{
			name: "return error from func",
			fields: fields{StoreEventFunc: func(
				aggregate eventsourced.Aggregate,
				event eventsourced.Event,
				sequenceNo int,
				newAggregate bool,
			) error {
				return errors.New("error")
			}},
			args: args{
				aggregate: &Dummy{},
				event:     &Added{},
			},
			wantErr: true,
		},
		{
			name:   "default return if func is nil",
			fields: fields{},
			args: args{
				aggregate: &Dummy{},
				event:     &Added{},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &Store{
				StoreEventFunc: tt.fields.StoreEventFunc,
			}
			err := m.StoreEvent(
				context.Background(),
				tt.args.aggregate,
				tt.args.event,
				tt.args.sequenceNo,
				tt.args.newAggregate,
			)
			if (err != nil) != tt.wantErr {
				t.Errorf("StoreEvent() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func TestStore_StoreSnapshot(t *testing.T) {
	type fields struct {
		StoreSnapshotFunc func(aggregate eventsourced.Aggregate, sequenceNo int) error
	}
	type args struct {
		aggregate  eventsourced.Aggregate
		sequenceNo int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "call func if defined",
			fields: fields{StoreSnapshotFunc: func(aggregate eventsourced.Aggregate, sequenceNo int) error {
				wantAggregate := &Dummy{}
				if !reflect.DeepEqual(aggregate, wantAggregate) {
					t.Errorf("StoreSnapshot() got %+v, want %+v", aggregate, wantAggregate)
				}
				return nil
			}},
			args: args{
				aggregate:  &Dummy{},
				sequenceNo: 10,
			},
			wantErr: false,
		},
		{
			name: "return error from func",
			fields: fields{StoreSnapshotFunc: func(aggregate eventsourced.Aggregate, sequenceNo int) error {
				return errors.New("error")
			}},
			args: args{
				aggregate:  &Dummy{},
				sequenceNo: 10,
			},
			wantErr: true,
		},
		{
			name:   "default return if func is nil",
			fields: fields{},
			args: args{
				aggregate:  &Dummy{},
				sequenceNo: 10,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &Store{
				StoreSnapshotFunc: tt.fields.StoreSnapshotFunc,
			}
			if err := m.StoreSnapshot(context.Background(), tt.args.aggregate, tt.args.sequenceNo); (err != nil) != tt.wantErr {
				t.Errorf("StoreSnapshot() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestStore_Events(t *testing.T) {
	type fields struct {
		EventsFunc func(fromId int, c chan eventsourced.Event, aggregateTypes ...eventsourced.Aggregate) error
	}
	type args struct {
		fromID         int
		c              chan eventsourced.Event
		aggregateTypes []eventsourced.Aggregate
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "call func if defined",
			fields: fields{EventsFunc: func(fromID int, c chan eventsourced.Event, aggregateTypes ...eventsourced.Aggregate) error {
				if fromID != 3 {
					t.Errorf("Events() got %d, want %d", fromID, 3)
				}
				want := []eventsourced.Aggregate{&Dummy{}}
				if !reflect.DeepEqual(aggregateTypes, want) {
					t.Errorf("Events() got %s, want %s", litter.Sdump(aggregateTypes), litter.Sdump(want))
				}
				return nil
			}},
			args: args{
				fromID:         3,
				c:              make(chan eventsourced.Event, 10),
				aggregateTypes: []eventsourced.Aggregate{&Dummy{}},
			},
			wantErr: false,
		},
		{
			name: "return error from func",
			fields: fields{EventsFunc: func(fromId int, c chan eventsourced.Event, aggregateTypes ...eventsourced.Aggregate) error {
				return errors.New("error")
			}},
			args: args{
				fromID:         3,
				c:              make(chan eventsourced.Event, 10),
				aggregateTypes: []eventsourced.Aggregate{&Dummy{}},
			},
			wantErr: true,
		},
		{
			name:   "default return if func is nil",
			fields: fields{},
			args: args{
				fromID:         3,
				c:              make(chan eventsourced.Event, 10),
				aggregateTypes: []eventsourced.Aggregate{&Dummy{}},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &Store{
				EventsFunc: tt.fields.EventsFunc,
			}
			if err := m.Events(context.Background(), tt.args.fromID, tt.args.c, tt.args.aggregateTypes...); (err != nil) != tt.wantErr {
				t.Errorf("Events() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestStore_MaxGlobalSequenceNo(t *testing.T) {
	type fields struct {
		RestoreSnapshotFunc func(aggregate eventsourced.Aggregate) (eventsourced.Snapshot, error)
		StoreSnapshotFunc   func(aggregate eventsourced.Aggregate, sequenceNo int) error
		GetEventsFunc       func(aggregate eventsourced.Aggregate, sinceSnapshot eventsourced.Snapshot) ([]eventsourced.EventWrapper, error)
		StoreEventFunc      func(
			aggregate eventsourced.Aggregate,
			event eventsourced.Event,
			sequenceNo int,
			newAggregate bool,
		) error
		GetAggregateRootsFunc func(aggregateType reflect.Type) ([]eventsourced.ID, error)
		EventsFunc            func(fromID int, c chan eventsourced.Event, aggregateTypes ...eventsourced.Aggregate) error
		MaxGlobalSeqNoFunc    func(ctx context.Context) (int, error)
	}
	tests := []struct {
		name    string
		fields  fields
		want    int
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "call func if defined",
			fields: fields{
				MaxGlobalSeqNoFunc: func(ctx context.Context) (int, error) {
					return 123, nil
				},
			},
			want:    123,
			wantErr: assert.NoError,
		},
		{
			name: "return error from func",
			fields: fields{
				MaxGlobalSeqNoFunc: func(ctx context.Context) (int, error) {
					return 0, fmt.Errorf("error")
				},
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "error")
			},
		},
		{
			name:    "default return if func is nil",
			fields:  fields{},
			want:    0,
			wantErr: assert.NoError,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &Store{
				RestoreSnapshotFunc:   tt.fields.RestoreSnapshotFunc,
				StoreSnapshotFunc:     tt.fields.StoreSnapshotFunc,
				GetEventsFunc:         tt.fields.GetEventsFunc,
				StoreEventFunc:        tt.fields.StoreEventFunc,
				GetAggregateRootsFunc: tt.fields.GetAggregateRootsFunc,
				EventsFunc:            tt.fields.EventsFunc,
				MaxGlobalSeqNoFunc:    tt.fields.MaxGlobalSeqNoFunc,
			}
			got, err := m.MaxGlobalSequenceNo(context.Background())
			if !tt.wantErr(t, err, "MaxGlobalSequenceNo()") {
				return
			}
			assert.Equalf(t, tt.want, got, "MaxGlobalSequenceNo()")
		})
	}
}

type Dummy struct {
	eventsourced.BaseAggregate
}

func (d *Dummy) Apply(eventsourced.Event) error {
	return nil
}

var _ eventsourced.Aggregate = &Dummy{}

type Added struct {
	eventsourced.EventAggregateId
	eventsourced.EventTime
	eventsourced.EventGlobalSequence
}

type snapshot struct {
	sequenceNo int
	when       time.Time
}

func (s *snapshot) SequenceNo() int {
	return s.sequenceNo
}

func (s *snapshot) When() time.Time {
	return s.when
}

var _ eventsourced.Snapshot = &snapshot{}
