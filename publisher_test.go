package mocks

import (
	"context"
	"errors"
	"fmt"
	"reflect"
	"testing"

	"gitlab.com/unboundsoftware/eventsourced/eventsourced"
)

func TestPublisher_Publish(t *testing.T) {
	type fields struct {
		PublishFunc func(event eventsourced.Event) error
	}
	type args struct {
		event eventsourced.Event
	}
	tests := []struct {
		name          string
		fields        fields
		args          args
		wantErr       bool
		wantPublished []eventsourced.Event
	}{
		{
			name: "uses func if defined",
			fields: fields{
				PublishFunc: func(event eventsourced.Event) error {
					wantEvent := &Added{}
					if !reflect.DeepEqual(event, wantEvent) {
						t.Errorf("Publish() got %+v, want %+v", event, wantEvent)
					}
					return errors.New("error")
				},
			},
			args:          args{event: &Added{}},
			wantErr:       true,
			wantPublished: nil,
		},
		{
			name:    "publishes to channel if func is not defined",
			fields:  fields{},
			args:    args{event: &Added{}},
			wantErr: false,
			wantPublished: []eventsourced.Event{
				&Added{},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Publisher{
				PublishFunc: tt.fields.PublishFunc,
			}
			if err := p.Publish(context.Background(), tt.args.event); (err != nil) != tt.wantErr {
				t.Errorf("Publish() error = %v, wantErr %v", err, tt.wantErr)
			}
			p.AssertPublished(t, tt.wantPublished)
		})
	}
}

func TestPublisher_AssertPublished(t *testing.T) {
	type args struct {
		want           []eventsourced.Event
		eventModifiers []EventModifier
	}
	tests := []struct {
		name    string
		args    args
		publish func(*Publisher)
		want    []string
	}{
		{
			name: "different lengths",
			publish: func(publisher *Publisher) {
				_ = publisher.Publish(context.Background(), &Added{})
			},
			want: []string{`Publisher() got = []eventsourced.Event{
  &mocks.Added{
    EventAggregateId: eventsourced.EventAggregateId{
      ID: "",
    },
    EventTime: eventsourced.EventTime{
      Time: time.Time{},
    },
    EventGlobalSequence: eventsourced.EventGlobalSequence{
      SeqNo: 0,
    },
  },
}, want nil`},
		},
		{
			name: "event modifiers",
			args: args{
				want: []eventsourced.Event{
					&Added{
						EventAggregateId: eventsourced.NewEventAggregateId("id1"),
					},
				},
				eventModifiers: []EventModifier{
					func(event eventsourced.Event) {
						event.(*Added).EventAggregateId = eventsourced.NewEventAggregateId("id1")
					},
				},
			},
			publish: func(publisher *Publisher) {
				_ = publisher.Publish(context.Background(), &Added{})
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Publisher{}
			tt.publish(p)
			temp := &MockT{}
			p.AssertPublished(temp, tt.args.want, tt.args.eventModifiers...)
			if !reflect.DeepEqual(temp.errors, tt.want) {
				t.Errorf("AssertPublished() got %+v, want %+v", temp.errors, tt.want)
			}
		})
	}
}

type MockT struct {
	testing.T
	errors []string
}

func (m *MockT) Errorf(format string, args ...interface{}) {
	m.errors = append(m.errors, fmt.Sprintf(format, args...))
}
