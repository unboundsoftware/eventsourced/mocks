# eventsourced mocks

[![GoReportCard](https://goreportcard.com/badge/gitlab.com/unboundsoftware/eventsourced/mocks)](https://goreportcard.com/report/gitlab.com/unboundsoftware/eventsourced/mocks) [![GoDoc](https://godoc.org/gitlab.com/unboundsoftware/eventsourced/mocks?status.svg)](https://godoc.org/gitlab.com/unboundsoftware/eventsourced/mocks) [![Build Status](https://gitlab.com/unboundsoftware/eventsourced/mocks/badges/main/pipeline.svg)](https://gitlab.com/unboundsoftware/eventsourced/mocks/commits/main)[![coverage report](https://gitlab.com/unboundsoftware/eventsourced/mocks/badges/main/coverage.svg)](https://gitlab.com/unboundsoftware/eventsourced/mocks/commits/main)

Package `mocks` provides mock implementations of the event store which can be used to unit test applications using the [eventsourced framework](https://gitlab.com/unboundsoftware/eventsourced/eventsourced).

Download:
```shell
go get gitlab.com/unboundsoftware/eventsourced/mocks
```
